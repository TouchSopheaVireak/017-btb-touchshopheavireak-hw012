
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { Component } from 'react'
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom';
import Menu from './Compoment/Menu';
import Home from './Compoment/Home';
import Account from './Compoment/Account';
import Auth from './Compoment/Auth';
import NotFound from './Compoment/NotFound';
import ReactRouter from './Compoment/ReactRouter';
import Post from './Compoment/Post';
import Video from './Compoment/Video';
import Welcome from './Compoment/Welcome';

export default class App extends Component {
  constructor(){
    super();
    this.state = {
      data: [{
        id: 1,
        description: "This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.",
        image: "https://vignette.wikia.nocookie.net/plunderer/images/e/ef/LichtPlundererAnime.jpg/revision/latest?cb=20200305023735" 
      },
      {
        id: 2,
        description: "This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.",
        image: "https://vignette.wikia.nocookie.net/plunderer/images/e/ef/LichtPlundererAnime.jpg/revision/latest?cb=20200305023735"
      },
      {
        id: 3,
        description: "This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.",
        image: "https://vignette.wikia.nocookie.net/plunderer/images/e/ef/LichtPlundererAnime.jpg/revision/latest?cb=20200305023735"
      }]
    }
  }
  
  render() {
    return (
    <div className="App">
        <BrowserRouter>
        
            <Menu />
            
            <Switch>
              <Route path = '/' exact component = {ReactRouter}/>
              <Route path = '/Home' render = {() => <Home data = {this.state.data} /> } />
              <Route path = '/Post/:id' render = {(props) => <Post {...props} data = {this.state.data} /> } />
              <Route path = '/Video' component = {Video} />
              <Route path = '/Account' component = {Account} />
              <Route path = '/Auth' component = {Auth} />
              <Route path = '/Welcome' component = {Welcome} />
              <Route path = '*' component = {NotFound}/>
            </Switch>
        </BrowserRouter>
    </div>
  );
}
}

