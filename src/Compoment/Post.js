import React from 'react'

function Post({match, data}) {
    // console.log(data);
    console.log(match.params.id);
    var dataview = data.find((d) => d.id == match.params.id);
    return (
        <div>
            <h1>This is content from post {dataview.id}</h1>
        </div>
    )
}

export default Post
