import {Form, Button, Row,Col, Container} from 'react-bootstrap';
import Input from './Input';
import React, { Component } from 'react'
import {Redirect , Switch, Route, BrowserRouter as Router, Link} from 'react-router-dom';
import Welcome from './Welcome';

export default class Auth extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                username: "",
                password: "", 
            },
        };
        
        this.handleName = this.handleName.bind(this);
        this.handlepassword = this.handlepassword.bind(this);
    };
    
    handleName(e) {
        let value = e.target.value;
        this.setState (
            prevState => ({
                data: {
                    ...prevState.data,
                    username: value
                }
            }),
        );
    };
    handlepassword(e) { 
        let value = e.target.value;
        this.setState (
            prevState => ({
                data: {
                    ...prevState.data,
                    password: value
                }
            }),
        );
    }
    onSubmit = () => {
        const {history} = this.props;
        if(this.state.data.username === "kshrd" && this.state.data.password === "1234") {
            if(history) history.push('/Welcome');
        }
    }

    render() {
        return (
            <Container>
            <Row style={{marginTop: "20px"}}>
                <Col md={4}>
                    <form>
                    <Input
                        inputType={"text"}
                        name={"name"}
                         value={this.state.data.username}
                        placeholder={"Enter your UserName"}
                         handleChange={this.handleName}
                        />{" "}
                    </form>
                </Col>
                <Col md={4}>
                    <form>
                    <Input
                        inputType={"text"}
                        name={"name"}
                         value={this.state.data.password}
                        placeholder={"Enter your Password"}
                         handleChange={this.handlepassword}
                        />{" "}
                    </form>
                </Col>
                <Col>
                <button onClick={this.onSubmit}>Login</button>
                </Col>
            </Row>
            </Container>
        )
    }
}

