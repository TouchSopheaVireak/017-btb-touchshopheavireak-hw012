import React from 'react';
import {Card, Button, Row, Col, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';

function Home({data}) {
    let allcard = data.map((data) =>
        <Col>
            <Card>
                <Card.Img variant="top" src = {data.image} />
                <Card.Body>
                    <Card.Title>Card Title</Card.Title>
                    <Card.Text>
                        {data.description}
                    </Card.Text>
                    <Link to = {`/Post/${data.id}`}><Button variant="light">Go somewhere</Button></Link>
                </Card.Body>
                <Card.Footer className="text-muted">2 days ago</Card.Footer>
            </Card>
        </Col>
    )
    console.log(data);
    return (
        <Container>
            <Row style={{marginTop: "40px"}}>          
                {allcard}               
            </Row>
        </Container>
    )
}

export default Home
