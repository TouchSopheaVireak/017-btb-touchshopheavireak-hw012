import React from 'react'
import {Link, Route, Switch} from 'react-router-dom';
import queryString from 'query-string';
import { Container } from 'react-bootstrap';

function Account(props) {

     let pv = queryString.parse(props.location.search);

    return (
        <Container>
            <h1>Accounts</h1>
            <ul>
                <li>
                    <Link to = {`/Account?name=Netfix`}>Netfix</Link>
                </li>
                <li>
                    <Link to = {`?name=ZillowGroup`}>Zillow Group</Link>
                </li>
                <li>
                    <Link to = {`?name=Yahoo`}>Yahoo</Link>
                </li>
                <li>
                    <Link to = {`?name=ModusCreate`}>Modus Create</Link>
                </li>
            </ul>
            <Switch>
               <Route path = {`/Account`}>
                    <h3>This name in the query string is"{pv.name}"</h3>
               </Route>

            </Switch>
        </Container>
    )
}

export default Account
