import React from 'react'
import {Container} from 'react-bootstrap';

function NotFound() {
    return (
        <Container>
            <h1>404 Page Not Founds</h1>
        </Container>
    )
}

export default NotFound
