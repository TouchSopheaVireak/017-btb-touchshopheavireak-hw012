import React from 'react'
import { Router, Link, BrowserRouter, Switch, Route } from 'react-router-dom'
import {Container} from 'react-bootstrap';

function Video({match}) {
    return (
        <Container>
            <ul>
                <li>
                    <Link to = {`${match.url}/Animation`}>Animation</Link>
                </li>
                <li>
                    <Link to = {`${match.url}/Moive`}>Moive</Link>
                </li>
            </ul>
            
            <Route
                path = {`${match.path}/Animation`}
                render = {({match}) => (
                    <div>
                        <h3>Animation Category</h3>
                        <ul>
                            <li>
                                <Link to = {`${match.url}/Action`}>Action</Link>
                            </li>
                            <li>
                                <Link to = {`${match.url}/Romance`}>Romance</Link>
                            </li>
                            <li>
                                <Link to = {`${match.url}/Comedy`}>Comedy</Link>
                            </li>
                        </ul>
                    </div>
                )}
            />
           <Route 
                path = {`${match.path}/Moive`}
                render = {({match}) => (
                    <div>
                        <h3>Moive Category</h3>
                        <ul>
                            <li>
                                <Link to = {`${match.url}/Adventure`}>Adventure</Link>
                            </li>
                            <li>
                                <Link to = {`${match.url}/Comedy`}>Comedy</Link>
                            </li>
                            <li>
                                <Link to = {`${match.url}/Crime`}>Crime</Link>
                            </li>
                            <li>
                                <Link to = {`${match.url}/Documentary`}>Documentary</Link>
                            </li>
                        </ul>
                    </div>
                )}
           />
           <Route 
                path = {`${match.path}/Animation/Action`}
                render = {({match}) => (
                    <div>
                       <h3>Action</h3>
                    </div>
                )}
           />
             <Route 
                path = {`${match.path}/Animation/Romance`}
                render = {({match}) => (
                    <div>
                       <h3>Romance</h3>
                    </div>
                )}
           />
            <Route 
                path = {`${match.path}/Animation/Comedy`}
                render = {({match}) => (
                    <div>
                       <h3>Comedy</h3>
                    </div>
                )}
           /> 

            <Route 
                path = {`${match.path}/Moive/Adventure`}
                render = {({match}) => (
                    <div>
                       <h3>Adventure</h3>
                    </div>
                )}
           />
             <Route 
                path = {`${match.path}/Moive/Comedy`}
                render = {({match}) => (
                    <div>
                       <h3>Comedy</h3>
                    </div>
                )}
           />
            <Route 
                path = {`${match.path}/Moive/Crime`}
                render = {({match}) => (
                    <div>
                       <h3>Crime</h3>
                    </div>
                )}
           /> 
           <Route 
                path = {`${match.path}/Moive/Documentary`}
                render = {({match}) => (
                    <div>
                       <h3>Documentary</h3>
                    </div>
                )}
           /> 
        </Container>
    )
}

export default Video

